#!/bin/bash

# Set environement variables for Google Cloud
export GCP_PROJECT_ID=$GCP_PROJECT_NAME
export GCP_SERVICE_ACCOUNT_NAME=$GCP_SERVICE_ACCOUNT
export GCP_SERVICE_ACCOUNT_KEYFILE=$GCP_KEY_JSON
export BASE64ENCODED_GCP_PROVIDER_CREDS=$(base64 $GCP_SERVICE_ACCOUNT_KEYFILE | tr -d "\n") # base64 encode the GCP credentials

export GKE_CLUSTER_NAME="autopilot-cluster-1"
export GKE_CLUSTER_ZONE="us-central1"

# Set environment vars for Crossplane installation
export CROSSPLANE_VERSION="1.2.0"
export CROSSPLANE_NS="crossplane-system"
